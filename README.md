# MAODV - RAODV
**Paper Implementation on NS2  - A Modified AODV Protocol Based on Nodes Velocity**

**By :**

     -  Rizaldy Primanta Putra
     -  5115100046
     -  Kelas JARNIL 2018

---
Konsep : 
Modifikasi AODV menjadi RAODV. Disini AODV tetap akan bekerja seperti aodv pada umumnya, hanya saja terjadi sedikit perubahan pada **RREQ** dan **RREP**. 
Yakni, terdapat penambahan parameter (variabel) baru yakni :
 - *V* (velocity/kecepatan) 
 - *lcf_sum* (total link stability coefficient)

	>Rumus :
	**lcf** (link stability coefficient) = **(Vi - Vy) * (Vi - Vy)**

Pada saat fase RREQ, 
**Pertama** source node akan menambahkan **V / kecepatan** pada node **ke-i** (dimulai dari node source), ***hop_count***, ***dan lcf_sum*** = 0. 
**Kedua** RREQ akan dibroadcast terhadap neighbor node. Ketika paket telah diterima,  neighbor node tersebut akan menghitung jumlah ***lcf_sum*** (sesuai rumus), jumlah ***hop_count***, dan mengupdate nilai ***V*** dengan kecepatan node tersebut.
**Ketiga**, packet RREQ ini akan terus dibroadcast dengan cara yang sama seperti diatas hingga ke destination node.
**Keempat**, destination node akan memilih routing path berdasar : 
 - Jika, total hop_count > 10, maka akan diambil path dengan nilai
   lcf_sum (link stability coefficient) terkecil
 - Jika total hop_count < 10, makan akan diambil path dengan total
   hop_count terkecil
   
   ---

# Working on Concept

Setting up **Velocity**. 
Kali ini kita akan menggunakan fungsi yang telah di define di **NS2**, kita akan mengedit 2 file di NS2, yakni : 

 Pada File `1. pathnya/ns-allinone-2.35/ns-2.35/common/ip.h` 
 Pada File `2. pathnya/ns-allinone-2.35/ns-2.35/mac/wireless-phy.cc`

Kita dapat mengakses ***Velocity*** pada physical layer, maka kita simpan ***Velocity*** nya di **IP Header**, dan di pass ke **Network Layer**. 
Maka kita tambah field/variabel baru di **IP Header** pada file **"ip.h"**


>1. Buka file di “ns-allinone-2.35/ns-2.35/common/ip.h”, 
>2. Lalu tambah line berikut

![Tambah di file ip.h, 1 line](https://www.bypass.id/img/ip-h-for-speed.png)

Setelah mengedit file Header tadi, 

Buka ”**ns-allinone-2.35/ns-2.35/mac/wireless-phy.cc**”. 
Pada file tersebut terdapat predefeined function untuk mengakses koordinat node yang terdefine "propagation.cc", yang di include kan di file “**wireless-phy.cc**”
